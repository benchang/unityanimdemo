using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour {

	protected Animator animator;
	
	// Use this for initialization
	void Start () {
		animator=GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.A))
		{
			animator.SetBool("Walking",true);	
		}
		if (Input.GetKeyUp(KeyCode.A))
		{
			animator.SetBool("Walking",false);	
		}
		if (Input.GetKeyDown(KeyCode.Space))
		{
			animator.SetBool("Jumping",true);
			Debug.Log("jumping!");
			
		}
		if (Input.GetKeyUp(KeyCode.Space))
		{
			animator.SetBool("Jumping",false);	
		}
		
	}
}
